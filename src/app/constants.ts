export const APP_LOADING_STATE_CHANGED = 'APP_LOADING_STATE_CHANGED';

export const GET_CITIES = 'GET_CITIES';
export const SET_CITIES = 'SET_CITIES';

export interface CitiesInterface {
    id: number;
    name: string;
    country: string;
}

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { AppStateInterface } from 'app/store';
import { RestService } from './RestService';

@Injectable()
export class UIService extends RestService {
    
    constructor(
        private store$: Store<AppStateInterface>,
        httpClient: HttpClient,
    ) {
        super(httpClient);
    }

    getWeather(id: number): Observable<any> {
        return this.get('/data/2.5/weather', { id, units: 'metric' });
    }

    getCities(): Observable<any> {
        return this.get('/assets/json/city.list.min.json', {});
    }

    isEmpty(obj) {

        if (obj == null) {
            return true;
        }
    
        if (obj.length > 0) {
            return false;
        }
        if (obj.length === 0) {
            return true;
        }
    
        if (typeof obj !== 'object') {
            return true;
        }
    
        for (const key in obj) {
            if (key) {
                return false;
            }
        }
    
        return true;
    }
}

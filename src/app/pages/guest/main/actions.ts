import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    GET_WEATHER, 
    SET_WEATHER,
    WeatherInterface,
} from './constants';

export function getWeather(id: number): ActionWithPayloadInterface {
    return {
        type: GET_WEATHER,
        payload: {
            id,
        },
    };
}

export function setWeather(data: WeatherInterface): ActionWithPayloadInterface {
    return {
        type: SET_WEATHER,
        payload: {
            data,
        },
    };
}

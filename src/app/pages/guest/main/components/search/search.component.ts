import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { mergeEffects } from '@ngrx/effects';
import { ISubscription } from 'rxjs/Subscription';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';

import { MainSearchEffects } from './$effects';
import { getCities } from 'app/actions';
import { UIService } from 'app/services/UIService';
import { CitiesInterface } from 'app/constants';
import { AppStateInterface } from 'app/store';

@Component({
    templateUrl: './search.template.html',
    styleUrls: [
        './search.style.scss',
    ],
    providers: [MainSearchEffects],
})

export class SearchComponent implements OnInit, OnDestroy {

    isLoading$: Observable<boolean>;
    data: CitiesInterface[];
    displayData: CitiesInterface[] = [];
    searchingText: string;

    private effectsSubscription: ISubscription;
    private dataSubscription: ISubscription;

    constructor(
        private store$: Store<AppStateInterface>,
        private route: Router,
        private effects$: MainSearchEffects,
    ) { }

    ngOnInit() {
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.isLoading$ = this.store$.select(state => state.isLoading);

        this.dataSubscription = this.store$.select(state => state.cities).subscribe((data) => {
            if (!data || data.length === 0) {
                this.store$.dispatch(getCities());
            }
            this.data = cloneDeep(data);
        });

    }

    ngOnDestroy() {
        this.effectsSubscription.unsubscribe();
        this.dataSubscription.unsubscribe();
    }

    goToCity(city) {
        this.route.navigate([`/view/${city.id}`]);
    }

    autocomplete() {
        if (this.searchingText.length > 3) {
            this.displayData = this.data.filter(row => row.name.toLocaleLowerCase().indexOf(this.searchingText.toLocaleLowerCase()) !== -1);
        } else {
            this.displayData = [];
        }
    }

}

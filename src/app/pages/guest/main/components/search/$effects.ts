import { Injectable } from '@angular/core';
import { debounceTime, tap, switchMap, catchError } from 'rxjs/operators';
import { Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import get from 'lodash/get';

import { GET_CITIES } from 'app/constants';
import { setAppLoadingState, setCities } from 'app/actions';
import { ActionWithPayloadInterface } from 'app/reducers';
import { MainGlobalEffects } from '../../$effects';

@Injectable()
export class MainSearchEffects extends MainGlobalEffects {
    @Effect() requestGetCities = this.actions$.pipe(
        ofType(GET_CITIES),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { } = action.payload;
            return this.uiService.getCities().pipe(
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
                switchMap((response: any) => {
                    this.store$.dispatch(setCities(response));
                    return of();
                }),
                catchError((error) => {
                    console.log(error);
                    return of(error);
                }),
            );
        }),
    );
}

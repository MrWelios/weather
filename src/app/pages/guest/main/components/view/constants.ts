import { AppStateInterface } from 'app/store';
import { WeatherInterface } from '../../constants';

export interface ViewStateInterface extends AppStateInterface {
    weather: WeatherInterface;
}

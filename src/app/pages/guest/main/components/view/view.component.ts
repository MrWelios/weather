import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { mergeEffects } from '@ngrx/effects';
import { ISubscription } from 'rxjs/Subscription';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';

import { MainViewEffects } from './$effects';
import { getWeather } from '../../actions';
import { SAVED_CITY, WeatherInterface } from '../../constants';
import { ViewStateInterface } from './constants';
import { weatherReducer } from '../../redusers';
import { UIService } from 'app/services/UIService';

@Component({
    templateUrl: './view.template.html',
    styleUrls: [
        './view.style.scss',
    ],
    providers: [MainViewEffects],
})

export class ViewComponent implements OnInit, OnDestroy {

    isLoading$: Observable<boolean>;

    id: number;
    data: WeatherInterface;
    isExist: boolean;
    selectedCity: {
        id: number;
        city: string;
    };
    cities: {
        id: number;
        city: string;
    }[] =  JSON.parse(localStorage.getItem(SAVED_CITY)) || [];

    private effectsSubscription: ISubscription;
    private dataSubscription: ISubscription;
    private timerId;

    constructor(
        private store$: Store<ViewStateInterface>,
        private route: Router,
        private effects$: MainViewEffects,
        private activatedRoute: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.store$.addReducer('weather', weatherReducer);
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.isLoading$ = this.store$.select(state => state.isLoading);

        this.activatedRoute.params.subscribe((params) => {
            if (params.id) {
                if (this.timerId) {
                    clearTimeout(this.timerId);
                }
                this.getWeather(params.id);
                this.timerId = setInterval(() => this.getWeather(params.id), 60000);
                this.id = params.id;
                this.isExist = this.cities.map(row => row.id).indexOf(params.id) !== -1;
            }
        });

        this.dataSubscription = this.store$.select(state => state.weather)
            .pipe(filter(data => data !== undefined)).subscribe(
                (data) => {
                    this.data = cloneDeep(data);
                },
            );
    }

    getWeather(id) {
        this.store$.dispatch(getWeather(id));
    }

    ngOnDestroy() {
        this.effectsSubscription.unsubscribe();
        this.store$.removeReducer('weather');
    }

    saveCity() {
        this.cities.push({
            id: this.id,
            city: this.data.city,
        });
        localStorage.setItem(SAVED_CITY, JSON.stringify(this.cities));
        this.isExist = true;
    }

    removeCity() {
        this.cities.splice(this.cities.map(row => row.id).indexOf(this.id), 1);
        localStorage.setItem(SAVED_CITY, JSON.stringify(this.cities));
        this.isExist = false;
    }

    selectCity() {
        this.route.navigate([`../${this.selectedCity.id}`], { relativeTo: this.activatedRoute });
    }

    goToSearch() {
        this.route.navigate(['/search']);
    }

}

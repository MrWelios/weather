import { Injectable } from '@angular/core';
import { debounceTime, tap, switchMap, catchError } from 'rxjs/operators';
import { Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import get from 'lodash/get';

import { GET_WEATHER } from '../../constants';
import { setAppLoadingState } from 'app/actions';
import { setWeather } from '../../actions';
import { ActionWithPayloadInterface } from 'app/reducers';
import { MainGlobalEffects } from '../../$effects';

@Injectable()
export class MainViewEffects extends MainGlobalEffects {
    @Effect() requestGetWeather = this.actions$.pipe(
        ofType(GET_WEATHER),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { id } = action.payload;
            return this.uiService.getWeather(id).pipe(
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
                switchMap((response: any) => {
                    this.store$.dispatch(setWeather({
                        weather: get(response, ['weather', 0, 'description'], ''),
                        temp: get(response, 'main.temp', 0),
                        city: get(response, 'name', ''),
                    }));
                    return of();
                }),
                catchError((error) => {
                    console.log(error);
                    return of(error);
                }),
            );
        }),
    );
}

import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppStateInterface } from 'app/store';
import { UIService } from 'app/services/UIService';

const LOADING_DEBOUNCE = 500;

@Injectable()
export class MainGlobalEffects {
    loadingDebounce = LOADING_DEBOUNCE;

    constructor(
        protected store$: Store<AppStateInterface>,
        protected actions$: Actions,
        protected uiService: UIService,
        protected route: Router,
        protected activatedRoute: ActivatedRoute,
    ) {}
}

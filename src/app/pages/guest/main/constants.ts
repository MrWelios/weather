export const GET_WEATHER = 'GET_WEATHER';
export const SET_WEATHER = 'SET_WEATHER';

export const SAVED_CITY = 'SAVED_CITY';

export interface WeatherInterface {
    city: string;
    temp: number;
    weather: string;
}

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { ViewComponent } from './components/view/view.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RoutingModule,
    ],
    providers: [
    ],
    declarations: [
        ViewComponent,
        SearchComponent,
    ],
})

export class MainModule {}

import { ActionWithPayloadInterface } from 'app/reducers';
import {
    SET_WEATHER,
    WeatherInterface,
} from './constants';

export const weatherReducer = (state: WeatherInterface, action: ActionWithPayloadInterface) => {
    if (action.type === SET_WEATHER) {
        return action.payload.data;
    }
    return state;
};

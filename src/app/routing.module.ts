import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ErrorPageComponent } from 'app/pages/components/error-page/index';
import { SimpleGuard } from './guards/SimpleGuard';

const routes: Routes = [
    { path: '', loadChildren: 'app/pages/guest/main/main.module#MainModule', canActivate: [SimpleGuard] },
    { path: '**', component: ErrorPageComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule { }

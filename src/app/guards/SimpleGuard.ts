import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import 'rxjs/add/observable/of';

import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
@Injectable()
export class SimpleGuard implements CanActivate {
    constructor(
        private router: Router,
        private store$: Store<AppStateInterface>,
        private uiService: UIService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return true;
    }

}

import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import 'rxjs/add/observable/of';

import { 
    setAppLoadingState, 
} from 'app/actions';
import { AppStateInterface } from 'app/store';

const LOADING_DEBOUNCE = 500;

@Injectable()
export class GlobalEffects {
    loadingDebounce = LOADING_DEBOUNCE;

    constructor(
        private store$: Store<AppStateInterface>,
        private actions$: Actions,
    ) {}
}

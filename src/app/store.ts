import { routerReducer, RouterReducerState } from '@ngrx/router-store';

import {
    isLoading,
    citiesReducer,
} from './reducers';
import { CitiesInterface } from './constants';

export interface AppStateInterface {
    isLoading: boolean;
    router: RouterReducerState;
    cities: CitiesInterface[];
}

export const commonReducers = {
    isLoading,
    router: routerReducer,
    cities: citiesReducer,
};

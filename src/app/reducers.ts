import { Action } from '@ngrx/store';

import {
    APP_LOADING_STATE_CHANGED,
    CitiesInterface,
    SET_CITIES,
} from './constants';

export interface ActionWithPayloadInterface extends Action {
    payload?: any;
}

export const isLoading = (state: boolean = false, action: ActionWithPayloadInterface) => {
    if (action.type === APP_LOADING_STATE_CHANGED) {
        return action.payload.isLoading;
    }

    return state;
};

export const citiesReducer = (state: CitiesInterface[] = [], action: ActionWithPayloadInterface) => {
    if (action.type === SET_CITIES) {
        return action.payload.data;
    }
    return state;
};

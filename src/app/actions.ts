import { ActionWithPayloadInterface } from 'app/reducers';
import {
    APP_LOADING_STATE_CHANGED,
    GET_CITIES,
    SET_CITIES,
    CitiesInterface,
} from './constants';

export function setAppLoadingState(isLoading: boolean = true): ActionWithPayloadInterface {
    return {
        type: APP_LOADING_STATE_CHANGED,
        payload: {
            isLoading,
        },
    };
}

export function getCities(): ActionWithPayloadInterface {
    return {
        type: GET_CITIES,
        payload: {
        },
    };
}

export function setCities(data: CitiesInterface[]): ActionWithPayloadInterface {
    return {
        type: SET_CITIES,
        payload: {
            data,
        },
    };
}
